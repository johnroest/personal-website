This is a starter template for [Learn Next.js](https://nextjs.org/learn).

Add a .env.local file at the root of your project.
NEXT_PUBLIC_MEASUREMENT_ID is used for google analytics.
Fill it with your tag.
