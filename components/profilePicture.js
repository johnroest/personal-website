import Image from 'next/image';

const Profile = () => (
  <Image
    src="/john.jpeg" // Route of the image file
    height={144} // Desired size with correct aspect ratio
    width={144} // Desired size with correct aspect ratio
    alt="John"
  />
);

export default Profile;