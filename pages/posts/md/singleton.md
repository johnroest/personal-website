---
title: 'Understanding the Singleton Pattern in Kotlin'
date: '2025-02-03'
ogImage: 'https://www.johnroest.nl/images/kotlin.png'
---

# Understanding the Singleton Pattern in Kotlin

The Singleton design pattern is one of the most commonly used creational patterns in software development. It ensures that a class has only one instance throughout the application's lifecycle and provides a global point of access to that instance. While powerful, this pattern should be used judiciously as it can introduce complexity and maintenance challenges.

## Why Use the Singleton Pattern?

Several scenarios can justify using the Singleton pattern:

*Resource Management*: Ensures only one instance of a resource-heavy object (like database connections or thread pools) exists at a time.

*Configuration Management*: Provides a central point for managing application-wide settings.

*Coordination*: Enables coordination between different parts of your application through a guaranteed single instance.

*Lazy Initialization*: Only creates the instance when first requested, optimizing resource usage.

## Implementing Singleton in Kotlin

Kotlin provides several ways to implement the Singleton pattern, each with its own trade-offs.

### 1. Basic Implementation Using `object`

The simplest and most idiomatic way to create a singleton in Kotlin is using the `object` declaration:

```kotlin
object Logger {
    fun log(message: String) {
        println("${System.currentTimeMillis()}: $message")
    }
}
```

This implementation:
- Ensures only one instance exists
- Provides thread-safe lazy initialization by default
- Is concise and clear in intent
- Cannot be inherited from or implement interfaces that contain properties

Usage is straightforward:

```kotlin
fun main() {
    Logger.log("Application started")
}
```

### 2. Parameterized Singleton

When you need a singleton that requires initialization parameters, you have two main approaches:

#### Option A: Using `by lazy` (Recommended)

```kotlin
class ConfigManager private constructor(val serverUrl: String) {
    companion object {
        private val instance by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
            ConfigManager("https://default.server.com")
        }
        
        fun getInstance(): ConfigManager = instance
    }
    
    fun getConfig(): String = "Configuration for $serverUrl"
}
```

#### Option B: Traditional Double-Checked Locking

```kotlin
class ConfigManager private constructor(val serverUrl: String) {
    companion object {
        @Volatile
        private var instance: ConfigManager? = null

        fun getInstance(serverUrl: String): ConfigManager {
            return instance ?: synchronized(this) {
                instance ?: ConfigManager(serverUrl).also { instance = it }
            }
        }
    }
}
```

The `by lazy` approach is preferred as it's more concise and less error-prone.

## Dependency Injection: A Modern Alternative

In modern Kotlin applications, dependency injection often provides a better solution than the Singleton pattern. Using a DI framework like Dagger Hilt or Koin:

```kotlin
@Singleton
class DatabaseService @Inject constructor(
    private val config: Config
) {
    fun query(sql: String): Result {
        // Implementation
    }
}
```

This approach:
- Maintains single instance behavior when needed
- Makes dependencies explicit
- Simplifies testing through dependency substitution
- Provides better lifecycle management

## Common Pitfalls and How to Avoid Them

### 1. Global State Issues

Singletons effectively create global state, which can lead to:
- Unpredictable behavior
- Difficult-to-track bugs
- Testing complications

**Solution**: Minimize mutable state in singletons and consider using dependency injection instead.

### 2. Testing Challenges

Singletons can make testing difficult because:
- State persists between tests
- Dependencies are hidden
- Parallel test execution becomes problematic

**Solution**: Design your singleton to be testable:

```kotlin
interface LoggerService {
    fun log(message: String)
}

object ProductionLogger : LoggerService {
    override fun log(message: String) {
        // Real implementation
    }
}

class TestLogger : LoggerService {
    val logs = mutableListOf<String>()
    
    override fun log(message: String) {
        logs.add(message)
    }
}
```

### 3. Memory Leaks

Singletons can cause memory leaks if they:
- Hold references to UI components
- Cache data indefinitely
- Maintain growing collections

**Solution**: Implement proper cleanup mechanisms and avoid storing UI references.

## Best Practices

1. **Prefer Dependency Injection**: Use DI frameworks when possible instead of manual singleton implementation.

2. **Use `object` Declaration**: When you need a singleton, prefer Kotlin's `object` declaration unless you have specific requirements that prevent its use.

3. **Avoid Mutable State**: Design singletons to be immutable when possible.

4. **Plan for Testing**: Design your singletons with testing in mind, possibly using interfaces for better mockability.

5. **Consider Lifecycle**: Implement cleanup methods if your singleton manages resources.

6. **Document Thread Safety**: Clearly document any thread-safety guarantees or requirements.

## Conclusion

While the Singleton pattern can be useful in specific scenarios, it should be used sparingly. Modern Kotlin applications often benefit more from dependency injection frameworks, which provide similar benefits with better maintainability and testability. When you do need a singleton, Kotlin's `object` declaration provides a clean, safe implementation for simple cases, while more complex scenarios can be handled using `by lazy` or dependency injection.

Remember that the best pattern is often the simplest one that meets your requirements while maintaining code clarity and testability.