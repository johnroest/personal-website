import fs from 'fs';
import path from 'path';
import matter from 'gray-matter';
import {marked} from "marked";
import {JSDOM} from "jsdom";

const postsDirectory = path.join(process.cwd(), 'pages/posts/md');

export function getSortedPostsData() {
    // Get file names under /posts
    const fileNames = fs.readdirSync(postsDirectory);
    const allPostsData = fileNames
        .filter((fileName) => {
            return fileName.includes('.md')
        })
        .map((fileName) => {
        // Remove ".md" from file name to get id
        const id = fileName.replace(/\.md$/, '');

        // Read markdown file as string
        const fullPath = path.join(postsDirectory, fileName);
        const fileContents = fs.readFileSync(fullPath, 'utf8');

        // Use gray-matter to parse the post metadata section
        const matterResult = matter(fileContents);

        if(id)
        // Combine the data with the id
        return {
            id,
            ...matterResult.data,
        };
    });
    // Sort posts by date
    return allPostsData.sort((a, b) => {
        if (a.date < b.date) {
            return 1;
        } else {
            return -1;
        }
    });
}

export function getAllPostIds() {
    const fileNames = fs.readdirSync(postsDirectory);
    return fileNames.map((fileName) => {
        return {
            params: {
                id: fileName.replace(/\.md$/, ''),
            },
        };
    });
}

export async function getPostData(id)  {
    // Construct the full path to the Markdown file
    const fullPath = path.join(postsDirectory, `${id}.md`);

    // Read the content of the Markdown file
    const fileContents = fs.readFileSync(fullPath, 'utf8');

    // Use gray-matter to parse the post metadata and content
    const matterResult = matter(fileContents);

    // Convert Markdown content to HTML using marked
    const processedContent = marked.parse(matterResult.content);

    // Use jsdom to manipulate the HTML
    const dom = new JSDOM(processedContent);
    const { document } = dom.window;

    // Find all <pre> elements and wrap them in a scrollable container
    const preElements = document.querySelectorAll('pre');
    preElements.forEach(pre => {
        const wrapper = document.createElement('div');
        wrapper.style.overflowX = 'scroll';
        wrapper.appendChild(pre.cloneNode(true));
        pre.replaceWith(wrapper);
    });

    // Serialize the manipulated document back to an HTML string
    const contentHtml = dom.serialize();

    // Combine the data with the id and contentHtml
    return {
        id,
        contentHtml,
        ...matterResult.data,
    };
}