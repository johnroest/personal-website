---
title: 'The C4 Architecture Model'
date: '2025-01-15'
ogImage: 'https://www.johnroest.nl/images/c4-model-diagram.png'
---

# The C4 Architecture Model

If you've ever tried explaining a complex software system to someone, you know the feeling: You start drawing boxes and arrows on a whiteboard, and before you know it, you've created something that looks more like abstract art than a system diagram. The C4 model changed all that for me, and I think it might do the same for you.

## What Makes C4 Different?

Created by Simon Brown out of frustration with inconsistent architecture diagrams, the C4 model isn't just another documentation framework – it's a way of thinking about and communicating software design that actually makes sense. It's named after its four levels of abstraction: Context, Containers, Components, and Code. Think of it like Google Maps for your software: you can zoom from a view of the entire city down to individual streets, but each view remains clear and useful.

### Core Principles

Before diving into the levels, let's understand what makes C4 tick:

- **Hierarchy over chaos**: Each level adds detail while maintaining context
- **Single responsibility**: Every element should tell you what it does in plain language
- **Consistency**: Same notation across all levels
- **Just enough detail**: No more, no less than what's needed at each level

## The Four Levels: A Deep Dive

### Level 1: Context - The 30,000 Foot View

The context diagram might seem oversimplified at first, but that's exactly its strength. It answers the most fundamental questions:

- What are we building?
- Who's using it?
- How does it fit into the existing world?

Here's what makes a good context diagram:

```
[External Users/Systems] <---> [Your System] <---> [External Dependencies]
```

Real-world example: For Netflix's streaming service, a context diagram might show:
- Viewers (users)
- Content Partners (movie studios)
- Payment Processors
- CDN Providers
- The Netflix Streaming Platform itself

### Level 2: Containers - The Technical Building Blocks

Don't let the name fool you – we're not talking about Docker here. In C4, containers are the independently deployable units that make up your system. They're the pieces you need to get your system running.

Key aspects to document:
- Technology choices
- Communication protocols
- Data storage
- Major responsibilities

Example: A modern web application might include:
```
+-------------------+     +------------------+     +------------------+
|  Single-Page App  | --> |  API Gateway     | --> |  Message Queue   |
|  (React/Next.js)  |     |  (Node.js)      |     |  (RabbitMQ)     |
+-------------------+     +------------------+     +------------------+
                               |
                               v
                    +----------------------+
                    |  PostgreSQL Cluster  |
                    |  (User/Order Data)   |
                    +----------------------+
```

### Level 3: Components - The Logical Building Blocks

This is where we start seeing the actual architecture of our application. Components are grouped chunks of functionality – think services, repositories, managers, etc.

Tips for effective component diagrams:
1. Group related functionality
2. Show clear boundaries
3. Highlight important interfaces
4. Document key dependencies

Example: Breaking down an API Gateway:
```
+-------------------------+    +----------------------+
|  Authentication Service |    | Rate Limiter        |
| - JWT validation       |    | - Request tracking   |
| - Role management      |    | - Quota enforcement  |
+-------------------------+    +----------------------+
            |                           |
            v                           v
    +------------------+        +-----------------+
    | Request Router   | -----> | Cache Manager   |
    | - API versioning |        | - Redis cache   |
    | - Load balancing |        | - TTL policies  |
    +------------------+        +-----------------+
```

### Level 4: Code - The Implementation Details

While you won't diagram every class, the code level is crucial for:
- Complex algorithms
- Critical business logic
- Reusable patterns
- Integration points

When to use code diagrams:
- During design reviews
- For onboarding documentation
- To explain complex workflows
- When documenting public APIs

## Practical Tips for Using C4

### Getting Started
1. Start with context – always
2. Use simple tools first (even pen and paper)
3. Focus on clarity over comprehensiveness
4. Get feedback from both technical and non-technical stakeholders

### Common Pitfalls
- **Too much detail**: Not everything needs to be documented
- **Inconsistent abstractions**: Stick to one level per diagram
- **Technical overload**: Use business language where possible
- **Static thinking**: Remember to document important runtime behavior

### Tools of the Trade
While you can start with any diagramming tool, some are particularly good for C4:
- Structurizr
- PlantUML with C4 extensions
- draw.io with C4 templates
- Mermaid (for simpler diagrams)

## Real-World Application

Let's say you're documenting a microservices-based e-commerce platform:

1. **Context**: Show the system interacting with customers, vendors, payment providers, and shipping services

2. **Containers**: Break it down into:
   - Customer-facing web app
   - Vendor portal
   - Order management service
   - Inventory service
   - Payment service
   - Each service's database

3. **Components**: Within the Order Management Service:
   - Order validator
   - Pricing engine
   - Inventory checker
   - Notification dispatcher
   - Payment coordinator

4. **Code**: Zoom into the Pricing Engine:
   - Price calculation classes
   - Discount rules
   - Tax calculators
   - Currency converters

## When to Use C4

C4 shines in several scenarios:
- **System documentation**: Creating living documentation that actually gets used
- **Architecture decisions**: Evaluating and communicating design choices
- **Onboarding**: Getting new team members up to speed quickly
- **Stakeholder communication**: Explaining technical decisions to non-technical audiences

## Beyond Documentation

The real power of C4 isn't just in documentation – it's in how it shapes thinking about system design. It encourages:
- Clear separation of concerns
- Thoughtful interface design
- Explicit dependency management
- Scalable system architecture

## Final Thoughts

The C4 model isn't perfect, but it's one of the most practical approaches to software architecture documentation I've encountered. It strikes that rare balance between being comprehensive enough for developers and clear enough for stakeholders.

Remember: The goal isn't to document everything – it's to document the right things in the right way. Start simple, focus on what matters, and let your documentation evolve with your system.

---

*Want to learn more? Check out Simon Brown's official C4 model documentation at [c4model.com](https://c4model.com) or join the growing community of practitioners sharing their experiences with this approach.*
