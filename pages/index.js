import Head from 'next/head';
import Layout, { siteTitle } from '../components/layout';
import utilStyles from '../styles/utils.module.css';
import Link from 'next/link';
import Date from '../components/date';
import GoogleAnalytics from '../components/GoogleAnalytics';

import { getSortedPostsData } from '../lib/posts';

export async function getStaticProps() {
    const allPostsData = getSortedPostsData();
    return {
        props: {
            allPostsData,
        },
    };
}

export default function Home ({ allPostsData }) {
  return (
    <Layout home>
      <Head>
        <title>{siteTitle}</title>
      </Head>
        <GoogleAnalytics />
      <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
          <header>
              <h1>Hi, I'm John Roest. Software Engineer with a passion for learning! 🚀</h1>
              <p>A dynamic software engineer driven by a relentless passion for crafting cutting-edge solutions. 💡</p>
              <div className="logo-container">
                  <a className={'logo-link black-white'} href="https://www.linkedin.com/in/john-roest-951a77a4/"
                     target="_blank">
                      <img className={"logo-image"}
                           src="https://content.linkedin.com/content/dam/me/business/en-us/amp/brand-site/v2/bg/LI-Bug.svg.original.svg"
                           alt="LinkedIn Logo"/>
                  </a>
                  <a className={'logo-link black-white'} href="https://gitlab.com/johnroest" target="_blank">
                      <img className="logo-image"
                           src="https://images.ctfassets.net/xz1dnu24egyd/1IRkfXmxo8VP2RAE5jiS1Q/ea2086675d87911b0ce2d34c354b3711/gitlab-logo-500.png"
                           alt="GitLab Logo"/>
                  </a>
                  <a className={'logo-link black-white'} href="https://www.youtube.com/@StackMasterJohn" target="_blank">
                      <img className="logo-image"
                           src="https://johnroest.nl/images/youtube.png"
                           alt="Youtube Logo"/>
                  </a>
              </div>
          </header>

          <section>
              <h2 className={utilStyles.headingLg}>Skills & Specializations:</h2>
              <ul>
                  <li>Languages: Swift, Kotlin, Java, JavaScript, TypeScript</li>
                  <li>Frameworks & Tools: React, React Native, SpringBoot, PHP, Node.js, Next.js</li>
                  <li>Databases: SQL, MySQL, PostgreSQL</li>
                  <li>Technologies: Kubernetes, Docker, ElasticSearch</li>
              </ul>
          </section>

          <section>
              <h2 className={utilStyles.headingLg}>Methodologies:</h2>
              <p>Leveraging Agile and Scrum methodologies, John collaborates seamlessly within teams to deliver
                  top-notch solutions efficiently. 🏗️</p>
          </section>

          <footer>
              <p>Embark on a journey through John Roest's digital playground to witness the fusion of artistry and
                  technology at its finest. 🔍</p>
          </footer>

          <h2 className={utilStyles.headingLg}>Blog</h2>
            <ul className={utilStyles.list}>
              {allPostsData.map(({ id, date, title }) => (
                  <li className={utilStyles.listItem} key={id}>
                      <Link href={`/posts/${id}`}>{title}</Link>
                      <br/>
                      <small className={utilStyles.lightText}>
                          <Date dateString={date}/>
                      </small>
                  </li>
              ))}
            </ul>
      </section>
    </Layout>
  );
}